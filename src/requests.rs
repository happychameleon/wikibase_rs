// requests.rs
//
// Copyright © 2018
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use std::str;

use crate::error::WikibaseError;
use crate::Configuration;

/// Make a request to a Wikibase.
///
/// Takes a url and returns a JSON result.
pub async fn wikibase_request(
    url: &str,
    configuration: &Configuration,
) -> Result<serde_json::Value, WikibaseError> {
    let api = configuration.api();
    let t = api
        .query_raw(url, &api.no_params(), "GET")
        .await
        .map_err(|_| WikibaseError::Request("Transfer failed".to_string()))
        .unwrap();
    serde_json::from_str(&t).map_err(|_| WikibaseError::Request("JSON parsing failed".to_string()))
}
