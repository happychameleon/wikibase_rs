#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use std::{error::Error, fmt};

#[derive(Debug, Clone)]
pub enum WikibaseError {
    Configuration(String),
    Request(String),
    Serialization(String),
    Validation(String),
}

impl Error for WikibaseError {}

impl fmt::Display for WikibaseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn wikibase_error() {
        let err = WikibaseError::Request("Oh no!".to_string());
        assert_eq!(format!("{}", &err), "Request(\"Oh no!\")");
    }
}
