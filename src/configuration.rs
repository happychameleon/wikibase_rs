#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::error::WikibaseError;
use crate::validate;

const USER_AGENT_BASE: &'static str = concat!("Wikibase-RS/", env!("CARGO_PKG_VERSION"));

/// Configurations for Wikibase RS
///
/// The Configuration struct holds all the parameters that can can be
/// customized. The api_url is the api endpoint or a particular Wikibase
/// instance. It defaults to Wikidata.
#[derive(Debug, Clone)]
pub struct Configuration {
    api_url: String,
    user_agent_prefix: String,
    api: mediawiki::api::Api,
}

impl Configuration {
    pub async fn new(user_agent_prefix: &str) -> Result<Configuration, WikibaseError> {
        let valid_user_agent_prefix = validate::validate_user_agent(&user_agent_prefix)?;
        let default_api_url = "https://www.wikidata.org/w/api.php";

        let api = match mediawiki::api::Api::new(default_api_url).await {
            Ok(api) => api,
            _ => {
                return Err(WikibaseError::Configuration(
                    "Can't create mediawiki API object".to_string(),
                ));
            }
        };

        let mut ret = Self {
            api_url: default_api_url.to_string(),
            user_agent_prefix: valid_user_agent_prefix.to_string(),
            api: api,
        };
        let ua = ret.user_agent();
        ret.api_mut().set_user_agent(ua);
        Ok(ret)
    }

    pub fn api_mut(&mut self) -> &mut mediawiki::api::Api {
        &mut self.api
    }

    pub fn api(&self) -> &mediawiki::api::Api {
        &self.api
    }

    pub fn api_url(&self) -> &str {
        &self.api_url
    }

    pub async fn set_api_url<S: Into<String>>(&mut self, api_url: S) {
        let new_api_url = api_url.into();
        if self.api_url == new_api_url {
            return;
        }
        self.api_url = new_api_url;
        self.api = mediawiki::api::Api::new(&self.api_url).await.unwrap();
        let ua = self.user_agent();
        self.api.set_user_agent(ua);
    }

    /// Returns the complete user agent that is used for requests
    ///
    /// The full user agent is a combination of the user agent prefix,
    /// that is set by a library and the Wikibase RS user agent.
    /// For example: `my_bot/0.2 Wikibase-RS/1.0.2`.
    pub fn user_agent(&self) -> String {
        format!("{} {}", &self.user_agent_prefix, USER_AGENT_BASE)
    }

    pub fn user_agent_prefix(&self) -> &str {
        &self.user_agent_prefix
    }
}
