#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use serde::{Deserialize, Serialize};

/// Sitelink
///
/// A sitelink contains the id of the connected site, which is usually a
/// combination of language code (skwiki, skwikiquote) and the project id or
/// for monolingual projects just the project-id (commonswiki, wikidatawiki).
///
/// The title of the page is stored as a string. A sitelink can also have
/// a list of badges that the page has. Badges are item-ids and for example
/// given to featured pages (Given as an ID e.g. "Q17437798").
///
/// For an overview of all allowed sites see:
/// https://www.wikidata.org/w/api.php?action=paraminfo&modules=wbsetlabel
///
/// # Example
///
/// ```
/// let sitelink = wikibase::SiteLink::new("dewiki", "Österreich", vec!["Q17437798".to_string()]);
/// ```
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SiteLink {
    badges: Vec<String>,
    site: String,
    title: String,
}

impl SiteLink {
    pub fn new<S: Into<String>>(site: S, title: S, badges: Vec<String>) -> SiteLink {
        let mut ret = Self {
            badges,
            site: site.into(),
            title: title.into(),
        };
        ret.badges.sort(); // Enforce sorted badges, for performance in PartialEq
        ret
    }

    pub fn site(&self) -> &String {
        &self.site
    }

    pub fn title(&self) -> &String {
        &self.title
    }

    pub fn badges(&self) -> &Vec<String> {
        &self.badges
    }
}

impl PartialEq for SiteLink {
    fn eq(&self, other: &SiteLink) -> bool {
        // REQUIRES `badges` to be sorted!
        self.site == other.site && self.title == other.title && self.badges == other.badges
    }
}

impl Eq for SiteLink {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sitelink() {
        let s = SiteLink::new("foo", "bar", vec!["zab".to_string(), "baz".to_string()]);
        assert_eq!(s.site(), "foo");
        assert_eq!(s.title(), "bar");
        assert_eq!(*s.badges(), vec!["baz", "zab"]); // Should be sorted automatically!
    }
}
