#![deny(
//    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use crate::error::WikibaseError;
use serde::ser::Serializer;
use serde::Serialize;

/// EntityType
///
/// Type of the a Wikidata entity. Is either Item or Property.
///
/// # JSON Mapping
///
/// Item = `item`
/// Property = `property`
///
/// # Example
///
/// ```
/// let entity_type = wikibase::EntityType::Item;
/// let item = wikibase::EntityType::new_from_str("Q256");
/// let property = wikibase::EntityType::new_from_str("P18");
/// ```
#[derive(Debug, Clone, PartialEq, Copy)]
pub enum EntityType {
    Item,
    Property,
    MediaInfo,
    Lexeme,
    LexemeSense,
    LexemeForm,
}

impl EntityType {
    pub fn new_from_str(type_string: &str) -> Option<EntityType> {
        match type_string {
            "item" => Some(EntityType::Item),
            "property" => Some(EntityType::Property),
            "mediainfo" => Some(EntityType::MediaInfo),
            "lexeme" => Some(EntityType::Lexeme),
            "sense" => Some(EntityType::LexemeSense),
            "form" => Some(EntityType::LexemeForm),
            _ => None,
        }
    }

    pub fn new_from_id(id_string: &str) -> Result<EntityType, WikibaseError> {
        let first_char = match id_string.chars().next() {
            Some(value) => value,
            None => {
                return Err(WikibaseError::Validation(
                    "Error getting first character of string".to_string(),
                ));
            }
        };

        match first_char {
            'P' => Ok(EntityType::Property),
            'Q' => Ok(EntityType::Item),
            'M' => Ok(EntityType::MediaInfo),
            'L' => Ok(EntityType::Lexeme),
            // TODO LexemeSense?
            // TODO LexemeForm?
            _ => Err(WikibaseError::Serialization(
                "Error matching entity type".to_string(),
            )),
        }
    }

    pub fn string_value(&self) -> String {
        match *self {
            EntityType::Item => "item".to_string(),
            EntityType::Property => "property".to_string(),
            EntityType::MediaInfo => "mediainfo".to_string(),
            EntityType::Lexeme => "lexeme".to_string(),
            EntityType::LexemeSense => "sense".to_string(),
            EntityType::LexemeForm => "form".to_string(),
        }
    }
}

impl Serialize for EntityType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let str_ptr = self.string_value();
        serializer.serialize_str(&str_ptr)
    }
}
