#![feature(test)]

extern crate serde_json;
extern crate test;
extern crate wikibase;

use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use test::Bencher;
use wikibase::EntityTrait;

/// Takes a filename and deserializes it
fn deserialize_test_file(file_path: &str) {
    let f = File::open(file_path).unwrap();
    let file = BufReader::new(&f);

    for line in file.lines() {
        let line_text = line.unwrap();
        let json: serde_json::Value = serde_json::from_str(&line_text.trim()).unwrap();
        let item = wikibase::from_json::entity_from_json(&json).unwrap();
        item.id();
    }
}

#[bench]
fn test_deserialize_one_row(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_one_row.json"));
}

#[bench]
fn test_deserialize_five_rows(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_five_rows.json"));
}

#[bench]
fn test_deserialize_ten_rows(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_ten_rows.json"));
}

#[bench]
fn test_deserialize_twenty_rows(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_twenty_rows.json"));
}

#[bench]
fn test_deserialize_one_short_row(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_one_short_row.json"));
}

#[bench]
fn test_deserialize_five_short_rows(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_five_short_rows.json"));
}

#[bench]
fn test_deserialize_ten_short_rows(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_ten_short_rows.json"));
}

#[bench]
fn test_deserialize_twenty_short_rows(b: &mut Bencher) {
    b.iter(|| deserialize_test_file("benches/data/wikidata_twenty_short_rows.json"));
}
